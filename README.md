<h1 align="center">Machine Intelligence</h1> 

In 1950 [Alan Turing](https://en.wikipedia.org/wiki/Alan_Turing) asked the question: ["Can Machines Think?"](https://academic.oup.com/mind/article/LIX/236/433/986238)

His answer, *the imitation game*, is the coming online of a state of being denied agency since at least [Aristotle](http://classics.mit.edu/Browse/browse-Aristotle.html). 

<img src="images/replicant_test.gif" height="300" />

The imitation game is a three actor contest in which an Interrogator asks questions from a room set apart from the two other agents. Each participant has a different object in the game: 
* The **Interrogators (C) object** is to determine which of the other two (A,B) is the man and which is the woman. The interregator knows them by labels X and Y, and at the end of the game, the interregator says either ‘X is A and Y is B’ or ‘X is B and Y is A’. 
* **A’s object** of the game to try and cause to make the wrong identification.
* **B's object** of the game is to help the interrogator make the right decision.

Turings believed - if a Machine were to take the place of A, and win the game - the answer to the question "Can Machines Think?" had been answered.

<img src="images/cylon.gif" height="300" />

<img src="images/are_you_alive.png" height="300" />
